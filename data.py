import re


__SEC_IN_MIN__ = 60
__SEC_IN_HR__ = __SEC_IN_MIN__ ** 2


class TimeSplit:
    def __init__(self, hour="0", minutes="0", seconds="0", ampm="", full_seconds=0):
        self.hour = int(hour)
        self.minutes = int(minutes)
        self.seconds = int(seconds)
        self.ampm = ampm
        if full_seconds:
            self.from_seconds(full_seconds)

    def am_pm_hr(self):
        return self.hour + 12 if (self.ampm + "").lower() == "pm" and self.hour < 12 else self.hour

    def in_seconds(self):
        return self.seconds \
            + self.minutes * __SEC_IN_MIN__ \
            + self.am_pm_hr() * __SEC_IN_HR__;

    def from_seconds(self, seconds):
        if not seconds and type(seconds) != int and seconds < 0:
            return
        self.hour = int(seconds / __SEC_IN_HR__)
        seconds -= self.hour * __SEC_IN_HR__
        self.minutes = int(seconds / __SEC_IN_MIN__)
        seconds -= self.minutes * __SEC_IN_MIN__
        self.seconds = seconds

    def __str__(self):
        return "{0:02d}:{1:02d}:{2:02d}".format(self.am_pm_hr(), self.minutes, self.seconds)

class TaskHour:
    def __init__(self, task="", date="", start="", end="", comment=""):
        self.task = task
        self.date = date
        self.start = start
        self.end = end
        self.comment = comment
    
    def get_time_diff(self):
        if not self.start or not self.end:
            return None
        time_a = make_time_split(self.start)
        time_b = make_time_split(self.end)
        return time_diff(time_a, time_b)

    def formatted_time_diff(self):
        answer = self.get_time_diff()
        if not answer:
            return ""
        hours = answer.am_pm_hr()
        if hours:
            hours = str(hours) + "h"
        minutes = answer.minutes
        if minutes:
            minutes = (" " if hours else "") + str(minutes) + "m"
        return (hours if hours else "") + (minutes if minutes else "");
        # return "{0:2d}h {1:2d}m".format(, answer.minutes)

    def __str__(self):
        return "Task: {} | When: {} | Time: {} | Description : {}".format(self.task, self.date, self.formatted_time_diff(), self.comment)



def split(txt):
    if not txt or not (type(txt) == str):
        return []
    return re.split(r'[\s\-\:\\\/]', txt)

def make_time_split(str_time):
    if not str_time or not (type(str_time) == str):
        return None
    splat = split(str_time)
    if not splat or len(splat) > 4:
        return None
    output = TimeSplit(splat[0], splat[1], splat[2])
    if len(splat) == 4:
        output.ampm = splat[3]
    return output

def time_diff(a, b):
    """Calculates difference in time contained in variable a and b
    
    Arguments:
        a {TimeSplit} -- first time sample
        b {TimeSplit} -- second time sample
    
    Returns:
        TimeSplit -- a value if valid. otherwise, None
    """
    if not ( type(a) == TimeSplit and type(b) == TimeSplit ):
        return None
    a_sec = a.in_seconds()
    b_sec = b.in_seconds()
    return TimeSplit(full_seconds= abs(b_sec - a_sec))
