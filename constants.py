
INPUT_FILE="hours.ods"
OUTPUT_FILE="worklog.csv"

SHEET_STARTING_NAME="HRS_GRID_"
SHEET_PATTERN=r"^{0}".format(SHEET_STARTING_NAME)

STARTING_WEEK_DATE_FORMAT='%m/%d/%y'
STARTING_WEEK_DATE_TARGET_FORMAT="%d/%m/%y"

START_DATE_FORMAT="%Y/%m/%d %H:%M:%S"
START_DATE_TARGET_FORMAT="%d/%m/%y %H:%M:%S"