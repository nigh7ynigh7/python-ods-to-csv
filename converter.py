from data import TimeSplit, TaskHour
import csv
import datetime
from constants import *

def create_csv(tasks):
    """Process the tasks into a CSV
    
    Arguments:
        tasks {[TaskHour]} -- Taskhours
    
    Raises:
        Exception: error if something went wrong
    """
    assert type(tasks) == list, 'Task must be of type list'
    with open(OUTPUT_FILE, mode='w') as worklog_file:
        writer = csv.writer(worklog_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)

        writer.writerow([ "Issue Key", "Date Created", "Time Spent", "Summary" ])
        
        last_task = None
        last_comment = None 
        for task in tasks:
            if type(task) != TaskHour:
                continue;
            
            current_task = task.task
            start_time = task.start
            total_time = task.formatted_time_diff()
            comment = task.comment if task.comment else ""
            dateWithTime=datetime.datetime.strptime("{} {}".format(task.date, start_time), START_DATE_TARGET_FORMAT)

            if not current_task and not last_task:
                raise Exception("Current task and whatnot cannot be empty")
            if current_task and last_task and last_task != current_task:
                last_comment = None
            if not current_task:
                current_task = last_task
            if not comment:
                comment = last_comment
            if not comment:
                comment = current_task

            writer.writerow([ 
                current_task,
                dateWithTime.strftime(START_DATE_FORMAT),
                total_time,
                comment
            ])

            last_comment = comment if comment else last_comment
            last_task = current_task if current_task else last_task

    