from converter import create_csv
from file_handler import load_and_process_spreadsheet

def main():
    tasks = load_and_process_spreadsheet()
    if not tasks:
        print('Nothing to do. Exiting')
        exit(0)
    
    print("Found {} task(s). Attempting to create csv".format(len(tasks)))
    create_csv(tasks)
    print("Completed. Enjoy your day.")
    

if __name__ == "__main__":
    main()