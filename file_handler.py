from odf.opendocument import load
from odf.table import *
from odf.element import Element
from odf.text import P
import re
from data import TaskHour, TimeSplit, split
import datetime
from constants import *

def get_cell(x, y, sheet):
    if not (type(x) == int) or not (type(y) == int):
        return None
    if y < 0 or x < 0:
        return None
    rows = sheet.getElementsByType( TableRow )
    if len(rows) <= y:
        return None
    count_y = 0
    output = None
    for row in rows:

        if count_y != y:
            count_y += 1
            continue

        cells = row.getElementsByType( TableCell )

        if len(cells) <= x:
            break
        
        output = cells[x]

        count_y += 1
    return output

def get_immediate_node(element, name):
    if not element:
        return None
    assert name, "Name cannot be empty"
    assert type(element) == Element, "Must be an element"

    children = element.childNodes

    if not children:
        return None
    
    for child in children:
        if child.tagName == name:
            return child

    return None

def get_immediate_text(element):
    node = get_immediate_node(element, "text:p")
    if node:
        return str(node)
    return ""

def get_cell_comment_text(element):
    node = get_immediate_node(element, 'office:annotation')
    if not node:
        return ""
    node = get_immediate_node(node, "text:p")
    if not node:
        return ""
    return str(node)


class DayRepresentation:
    def __init__(self, registries=[]):
        assert type(registries) == list, "Registries must be a list"
        self.registries = registries[:]
    def add_registry(self, task, start, end, comment):
        self.registries.append((task, start, end, comment))

class WeekRepresentation:
    def __init__(self, week_of):
        self.week_of = week_of
        self.day_registries = []
    def add_day(self, day):
        assert type(day) == DayRepresentation and day, "Day is invalid or empty"
        self.day_registries.append(day)

def fetch_fields(sheet):
    assert sheet and type(sheet) == Element, "Input value of `sheet` is not an odfpy Element object"

    week = WeekRepresentation(str(get_cell(1, 0, sheet)))

    starting_points = [(1, x) for x in range(1, 33, 5)]

    for point in starting_points:
        day = DayRepresentation()
        for registry in range(14):
            task = get_cell(registry + point[0], point[1], sheet)
            start = get_cell(registry + point[0], point[1] + 1, sheet)
            end = get_cell(registry + point[0], point[1] + 2, sheet)
            if not start or not str(start).strip() or not end or not str(end).strip():
                continue
            day.add_registry(get_immediate_text(task), str(start), str(end), get_cell_comment_text(task))
        week.add_day(day)

    return week



def weeks_to_task_hours(weeks):
        
    if not weeks or len(weeks) == 0:
        return [];
    
    output = []

    for week in weeks:
        assert type(week) == WeekRepresentation
        
        current_date = datetime.datetime.strptime(week.week_of,STARTING_WEEK_DATE_FORMAT)
        for day in week.day_registries:
            for registry in day.registries:
                
                output.append(TaskHour(
                    registry[0],
                    current_date.strftime(STARTING_WEEK_DATE_TARGET_FORMAT),
                    registry[1],
                    registry[2],
                    registry[3]
                ))
            current_date += datetime.timedelta(1)

            
    return output


def load_and_process_spreadsheet(fileName=INPUT_FILE):
    """loads any LibreOffice/OpenOffice odf file (with the expected structure) and it will generate a list of TaskHour
    
    Keyword Arguments:
        name {str} -- file name (default: {"hours.ods"})
    
    Raises:
        e: Any error? You get to deal with it!
    
    Returns:
        [TaskHour] -- list of tasks
    """

    tasks = []
    try:
        document = load(fileName)
        spreadsheet = document.spreadsheet
        sheets = spreadsheet.getElementsByType( Table )
        sheets.reverse()
        weeks = []
        for sheet in sheets:
            name = sheet.getAttribute('name')
            if not re.match(SHEET_PATTERN, name, re.IGNORECASE):
                continue;
            weeks.append(fetch_fields(sheet))
        
        tasks = weeks_to_task_hours(weeks)

    except Exception as e:
        raise e
    
    return tasks

